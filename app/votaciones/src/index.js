import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import Button from "./components/Button";
import Statistics from "./components/Statistics";
import {Content, VoteButton} from "./elements/Form";

const App = () => {
    const [good, setGood] = useState(0)
    const [neutral, setNeutral] = useState(0)
    const [bad, setBad] = useState(0)
    const [punctuation, setPunctuation] = useState(0)

    const handleGood = () => {
        setGood(good + 1)
        setPunctuation(punctuation + 1)
    }

    const handleNeutral = () => {
        setNeutral(neutral + 1)
        setPunctuation(punctuation)
    }

    const handleBad = () => {
        setBad(bad + 1)
        setPunctuation(punctuation - 1)
    }

    return (
        <Content>
            <h1>Give Feedback</h1>
            <VoteButton>
                <Button event={handleGood} comment="Good"/>
                <Button event={handleNeutral} comment="Neutral"/>
                <Button event={handleBad} comment="Bad"/>
            </VoteButton>
            <Statistics good={good} neutral={neutral} bad={bad} punctuation={punctuation} />
        </Content>
    );
}

ReactDOM.render(<App />,
    document.getElementById('root')
)

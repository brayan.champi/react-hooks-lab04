import styled from "styled-components";

const Content = styled.div`
  width: 50%;
  margin: auto;
  padding: 10px;
  text-align: center;
  background-color: #a9fff3;
`;

const StatisticsContent = styled.div`
    display: flex;
    flex-direction: column;
`;

const VoteButton = styled.div`
    display: flex;
    justify-content: center;
`;

const BtnStyle = styled.button`
    margin: 7px;
    padding: 7px;
    width: 80px;
    border-radius: 12px;
    cursor: pointer;
    outline: none;
    border: 2px solid #008CBA;
    background-color: #e7e7e7;
    
    &:hover {
      transition: ease all 0.4s;
      background-color: #008CBA;
    }
`;



export {Content, StatisticsContent, VoteButton, BtnStyle};

import React from 'react';

const Statistic = (props) => {
    const {text, value} = props
    return (
        <div>
            <span><b>{text}</b>: {value}%</span>
        </div>
    );
};

export default Statistic;

import React from 'react';
import {StatisticsContent} from "../elements/Form";
import Statistic from "./Statistic";

const Statistics = (props) => {
    const {good, neutral, bad, punctuation} = props
    const all = good + neutral + bad
    const average = (punctuation / all).toFixed(2)
    const positive = (good / all * 100).toFixed(2)
    return (
        <div>
            {
                all === 0 ? <p>No feedback given</p>
                :
                <>
                    <h2>Statistics</h2>
                    <StatisticsContent>
                        <Statistic text='Good' value={good}/>
                        <Statistic text='Neutral' value={neutral}/>
                        <Statistic text='Bad' value={bad}/>
                        <Statistic text='All' value={all}/>
                        <Statistic text='Average' value={all && average}/>
                        <Statistic text='Positive' value={all && positive}/>
                    </StatisticsContent>
                </>
            }
        </div>
    );
};

export default Statistics;

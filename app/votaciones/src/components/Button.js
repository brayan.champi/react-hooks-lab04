import React from 'react';
import {BtnStyle} from "../elements/Form";

const Button = (props) => {
    const {comment, event} = props
    return (
        <div>
            <BtnStyle onClick={event}>{comment}</BtnStyle>
        </div>
    );
}

export default Button;

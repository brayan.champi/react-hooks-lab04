import React, {useState} from 'react'
import ReactDOM from 'react-dom'
import {BtnStyle, Content, VoteButton} from "./elements/Form";

const App = (props) => {
    const [selected, setSelected] = useState(0)
    const [vote, setVote] = useState([0])
    const [mostVotes, setMostVotes] = useState(0)

    const handleNext = () => {
        setSelected(Math.floor(Math.random() * anecdotes.length));
    }

    const handleVote = () => {
        if (!vote[selected]) { vote[selected] = 0 }
        vote[selected] = vote[selected] + 1
        setVote(vote)
        let index = 0
        let max = 0
        while (index < anecdotes.length) {
            if (vote[index]) {
                if (vote[index] > max) { max = vote[index]; setMostVotes(index); }
            }
            index = index + 1
        }
    }

    return (
        <Content>
            <h1>Anecdote of the day</h1>
            {props.anecdotes[selected]}
            <div>
                <span>has {vote[selected]} votes</span>
            </div>
            <VoteButton>
                <BtnStyle onClick={handleVote}>vote</BtnStyle>
                <BtnStyle onClick={handleNext}>next anecdote</BtnStyle>
            </VoteButton>
            <div>
                <h1>Anecdote with most votes</h1>
                {
                    mostVotes > 0 ?
                        <div>
                            <span>{props.anecdotes[mostVotes]}</span>
                            <p>has {vote[mostVotes]} votes</p>
                        </div>
                        :
                        <span>No votes</span>
                }
            </div>
        </Content>
    )
}

const anecdotes = [
    'If it hurts, do it more often',
    'Adding manpower to a late software project makes it later!',
    'The first 90 percent of the code accounts for the first 90 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time.',
    'Any fool can write code that a computer can understand. Good programmers write code that humans can understand.',
    'Premature optimization is the root of all evil.',
    'Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it.'
]

ReactDOM.render(
    <App anecdotes={anecdotes}/>
    ,
    document.getElementById('root')
)
